# Marvel List - test for front-end developer Softplan

Project developed with Reactjs, with Continuous Integration and Continuos Delivery.
The application consists of listing Marvel characters being able to filter by name and being able to edit the character on the client side.

### Installation and run

```sh
$ cd marvel-list
$ yarn
$ yarn start
```

Runs the app in the development mode.<br />

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Run Tests

```sh
$ yarn test
```

### Url for aplication in Production

Open [https://marvel-list.netlify.app](https://marvel-list.netlify.app) to view it in the browser.