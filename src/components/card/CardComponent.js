import React from 'react';
import PropTypes from 'prop-types';

import history from '../../services/history';

import { Container } from './styles';

function CardComponent({ id, name, imageUrl }) {
  const handleClick = () => {
    history.push(`/${id}`);
  };

  const shortName = () => {
    const SIZE = 30;
    if (name.length > SIZE) {
      const newName = `${name.substring(0, SIZE - 3)} ...`;
      return newName;
    }
    return name;
  };

  return (
    <Container onClick={handleClick}>
      <div>
        <img src={imageUrl} alt="character" />
      </div>
      <section>
        <p>{shortName()}</p>
      </section>
    </Container>
  );
}

CardComponent.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
};

export default CardComponent;
