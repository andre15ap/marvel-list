import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import history from '../../services/history';

import CharacterDetailComponent from '../../components/characterDetail/CharacterDetailComponent';
import ModalCardFormComponent from '../../components/modalCardForm/ModalCardFormComponent';

import { Container } from './styles';

function DetailPage() {
  const [openForm, setOpenForm] = useState(false);
  const characters = useSelector((state) => state.character.characters);
  const { id } = useParams();
  const character = characters.find((value) => value.id === Number(id));

  const handleReload = () => {
    history.push('/');
  };

  window.addEventListener('load', handleReload);
  window.scrollTo(0, 0);

  return (
    <Container background={!openForm}>
      {character && (
        <CharacterDetailComponent
          character={character}
          handleEdit={() => setOpenForm(true)}
        />
      )}
      {openForm && (
        <ModalCardFormComponent
          close={() => setOpenForm(false)}
          character={character}
        />
      )}
    </Container>
  );
}

export default DetailPage;
