/* eslint-disable no-undef */
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';

import ModalCardFormComponent from '../../components/modalCardForm/ModalCardFormComponent';
import { editList } from '../../store/modules/character/actions';

jest.mock('react-redux');

describe('ModalCardForm Component', () => {
  it('Should render modal form of character', () => {
    useSelector.mockImplementation((call) =>
      call({
        character: {
          characters: [
            {
              id: 1,
              name: 'Spider Man',
              description: 'super',
              thumbnail: { path: 'http:link.com', extension: 'png' },
              series: { items: [{ name: 'serie spider' }] },
            },
            {
              id: 2,
              name: 'Hulk',
              description: 'incrible',
              thumbnail: { path: 'http:link.com', extension: 'png' },
              series: { items: [{ name: 'serie hulk' }] },
            },
          ],
          page: 1,
        },
      })
    );

    const characters = useSelector((state) => state.character.characters);
    const fakeCharacter = characters[0];

    global.scrollTo = jest.fn();
    const close = jest.fn();
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);

    const { getByLabelText, getByTestId } = render(
      <ModalCardFormComponent character={fakeCharacter} close={close} />
    );

    const inputName = getByLabelText('Nome');

    fireEvent.change(inputName, { target: { value: 'Spiner Men Editado' } });

    const form = getByTestId('form');
    fireEvent.submit(form);

    const newCharacters = characters.map((value) => {
      if (value.id === fakeCharacter.id) {
        return {
          ...value,
          name: 'Spiner Men Editado',
          image: `${value.thumbnail.path}.${value.thumbnail.extension}`,
        };
      }
      return value;
    });

    expect(dispatch).toHaveBeenCalledWith(editList(newCharacters));
    expect(close).toBeCalledTimes(1);
  });

  // it('Should render empty list of characters', () => {
  //   useSelector.mockImplementation((call) =>
  //     call({
  //       character: {
  //         characters: [],
  //         page: 1,
  //       },
  //     })
  //   );

  //   const dispatch = jest.fn();
  //   useDispatch.mockReturnValue(dispatch);

  //   const { getByText } = render(<CardListComponent />);

  //   const empty = getByText('Lista Vazia');

  //   expect(empty).toBeTruthy();
  //   expect(dispatch).toHaveBeenCalledWith(listRequest(1));
  // });
});
