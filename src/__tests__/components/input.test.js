/* eslint-disable no-undef */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import InputComponent from '../../components/input/InputComponent';

describe('Input Component', () => {
  it('Should call onchange function', () => {
    const funcAction = jest.fn();
    const { getByLabelText } = render(
      <InputComponent
        name="input"
        labelName="Input"
        value="teste"
        handleChange={funcAction}
      />
    );

    const input = getByLabelText('Input');

    fireEvent.change(input, { target: { value: 'a' } });
    expect(funcAction).toBeCalledWith('a');
    fireEvent.change(input, { target: { value: 'b' } });
    expect(funcAction).toBeCalledWith('b');
  });
});
