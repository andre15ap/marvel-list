/* eslint-disable no-undef */
import React from 'react';
import { useSelector } from 'react-redux';
import { render } from '@testing-library/react';

import FilterComponent from '../../components/filter/FilterComponent';

jest.mock('react-redux');

describe('Filter Component', () => {
  it('Should render buttons and input value initial', () => {
    useSelector.mockImplementation((call) =>
      call({
        character: {
          name: 'Spider',
          page: 1,
        },
      })
    );

    const { getByText, getByLabelText } = render(<FilterComponent />);
    const input = getByLabelText('Filtrar por Nome');
    const btnSearch = getByText('Buscar');
    const btnClean = getByText('Limpar');
    expect(input.value).toBe('Spider');
    expect(btnSearch).toBeTruthy();
    expect(btnClean).toBeTruthy();
  });

  it('Should render buttons and input value initial empty', () => {
    useSelector.mockImplementation((call) =>
      call({
        character: {
          name: '',
          page: 1,
        },
      })
    );

    const { getByText, getByLabelText } = render(<FilterComponent />);
    const input = getByLabelText('Filtrar por Nome');
    const btnSearch = getByText('Buscar');
    const btnClean = getByText('Limpar');
    expect(input.value).toBe('');
    expect(btnSearch).toBeTruthy();
    expect(btnClean).toBeTruthy();
  });
});
