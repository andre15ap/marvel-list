import React from 'react';

import { Container, Load } from './styles';

function LoadingComponent() {
  return (
    <Container>
      <Load data-testid="loading" />
    </Container>
  );
}

export default LoadingComponent;
