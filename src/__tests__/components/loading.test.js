/* eslint-disable no-undef */
import React from 'react';
import { render } from '@testing-library/react';

import LoadingComponent from '../../components/loading/LoadingComponent';

describe('Loading Component', () => {
  it('Should render loading', () => {
    const { getByTestId } = render(<LoadingComponent />);
    const loading = getByTestId('loading');
    expect(loading).toBeTruthy();
  });
});
