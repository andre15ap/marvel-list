import React from 'react';

import CardListComponent from '../../components/cardList/CardListComponent';
import FilterComponent from '../../components/filter/FilterComponent';
import { Container, Content } from './styles';

function HomePage() {
  return (
    <Container>
      <Content>
        <FilterComponent />
        <CardListComponent />
      </Content>
    </Container>
  );
}

export default HomePage;
