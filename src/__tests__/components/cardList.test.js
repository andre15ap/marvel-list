/* eslint-disable no-undef */
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { render } from '@testing-library/react';

import CardListComponent from '../../components/cardList/CardListComponent';
import { listRequest } from '../../store/modules/character/actions';

jest.mock('react-redux');

describe('CardList Component', () => {
  it('Should render card list of characters', () => {
    useSelector.mockImplementation((call) =>
      call({
        character: {
          characters: [
            {
              id: 1,
              name: 'Spider Man',
              thumbnail: { path: 'http:link.com', extension: 'png' },
            },
            {
              id: 2,
              name: 'Hulk',
              thumbnail: { path: 'http:link.com', extension: 'png' },
            },
          ],
          page: 1,
        },
      })
    );

    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);

    const { getByText } = render(<CardListComponent />);

    const itemOne = getByText('Spider Man');
    const itemTwo = getByText('Hulk');

    expect(itemOne).toBeTruthy();
    expect(itemTwo).toBeTruthy();
    expect(dispatch).toHaveBeenCalledWith(listRequest(1));
  });

  it('Should render empty list of characters', () => {
    useSelector.mockImplementation((call) =>
      call({
        character: {
          characters: [],
          page: 1,
        },
      })
    );

    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);

    const { getByText } = render(<CardListComponent />);

    const empty = getByText('Lista Vazia');

    expect(empty).toBeTruthy();
    expect(dispatch).toHaveBeenCalledWith(listRequest(1));
  });
});
