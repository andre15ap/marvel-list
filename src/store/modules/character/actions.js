import TYPES from './types';

export function listRequest(page, name) {
  return {
    type: TYPES.CHARACTER_LIST_REQUEST,
    payload: { page, name },
  };
}

export function listSuccess(data) {
  return {
    type: TYPES.CHARACTER_LIST_SUCCESS,
    payload: { data },
  };
}

export function listFailure() {
  return {
    type: TYPES.CHARACTER_LIST_FAILURE,
  };
}

export function setParams(page, name = '') {
  return {
    type: TYPES.CHARACTER_SET_PARAMS,
    payload: { page, name },
  };
}

export function editList(data) {
  return {
    type: TYPES.CHARACTER_EDIT_LIST,
    payload: { data },
  };
}
