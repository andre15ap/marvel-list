import styled from 'styled-components';

import COLORS from '../../constants/colors';

export const Container = styled.div`
  width: 100%;
  min-height: 100%;
  padding-top: 60px;
  display: flex;
  justify-content: center;
  background: ${COLORS.BACKGROUND};
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 900px;
  background: ${COLORS.WHITE};
  margin: 10px 0;
  border-radius: 5px;
`;
