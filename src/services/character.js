// import imageDefault from '../assets/images/default.png';

function setCharacters(data) {
  localStorage.setItem('@characters', JSON.stringify(data));
}

function getCharacters() {
  const characters = localStorage.getItem('@characters');
  if (characters) {
    return JSON.parse(characters);
  }
  return [];
}

function setCharacter(character) {
  const characters = getCharacters();
  const item = characters.find((value) => value.id === character.id);
  if (item) {
    const newCharacters = characters.map((value) => {
      return value.id === character.id ? character : value;
    });
    setCharacters(newCharacters);
    return;
  }
  const newCharacters = [...characters, character];
  setCharacters(newCharacters);
}

function compareEdited(characters) {
  const editedCharacters = getCharacters();

  const newCharacters = characters.map((value) => {
    const newCharacter = editedCharacters.find((edit) => value.id === edit.id);
    return newCharacter || value;
  });

  return newCharacters;
}

function cleanCharacters() {
  localStorage.clear();
}

function getImageUrl(character) {
  if (character.image) {
    return character.image;
  }
  return `${character.thumbnail.path}.${character.thumbnail.extension}`;
}

export {
  setCharacters,
  setCharacter,
  getCharacters,
  compareEdited,
  cleanCharacters,
  getImageUrl,
};
