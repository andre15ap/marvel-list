import React from 'react';
import PropTypes from 'prop-types';
import history from '../../services/history';

import logo from '../../assets/svgs/logo.svg';

import { Container, Content } from './styles';

function HeaderComponent({ title }) {
  const handleClick = () => {
    history.push('/');
  };
  return (
    <Container>
      <Content>
        <button type="button" onClick={handleClick} title="Inicio">
          <img src={logo} alt="logo" />
        </button>

        <aside>
          <button type="button" onClick={handleClick}>
            {title}
          </button>
        </aside>
      </Content>
    </Container>
  );
}

HeaderComponent.propTypes = {
  title: PropTypes.string,
};

HeaderComponent.defaultProps = {
  title: 'Marvel List',
};

export default HeaderComponent;
