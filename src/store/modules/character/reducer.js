import TYPES from './types';
import PAGES from '../../../constants/pagination';

export const INITIAL_STATE = {
  characters: [],
  loading: false,
  page: 1,
  hasMore: false,
  name: '',
};

function character(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TYPES.CHARACTER_LIST_REQUEST:
      return {
        ...state,
        page: action.payload.page,
        name: action.payload.name || '',
        characters: action.payload.page === 1 ? [] : state.characters,
        loading: true,
      };
    case TYPES.CHARACTER_LIST_SUCCESS:
      return {
        ...state,
        characters: [...state.characters, ...action.payload.data],
        page: state.page + 1,
        loading: false,
        hasMore: !(action.payload.data.length < PAGES),
      };
    case TYPES.CHARACTER_LIST_FAILURE:
      return { ...state, loading: false };
    case TYPES.CHARACTER_EDIT_LIST:
      return { ...state, characters: [...action.payload.data] };
    case TYPES.CHARACTER_SET_PARAMS:
      return { ...state, page: action.payload.page, name: action.payload.name };
    default:
      return state;
  }
}

export default character;
