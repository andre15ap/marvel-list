/* eslint-disable no-undef */
import PAGE from '../../constants/pagination';
import character, {
  INITIAL_STATE,
} from '../../store/modules/character/reducer';
import {
  editList,
  listRequest,
  listSuccess,
  listFailure,
} from '../../store/modules/character/actions';

describe('Reducer character', () => {
  it('List Request', () => {
    const state = character(INITIAL_STATE, listRequest(1));
    expect(state).toStrictEqual({
      page: 1,
      name: '',
      characters: [],
      loading: true,
      hasMore: false,
    });
  });

  it('List Success has more', () => {
    const data = [];
    for (let i = 0; i < PAGE; i += 1) {
      data.push({ id: i + 1, name: 'character' });
    }
    const state = character(INITIAL_STATE, listSuccess(data));
    expect(state).toStrictEqual({
      page: 2,
      name: '',
      characters: data,
      loading: false,
      hasMore: true,
    });
  });

  it('List Success no has more', () => {
    const data = [];
    for (let i = 0; i < PAGE - 1; i += 1) {
      data.push({ id: i + 1, name: 'character' });
    }
    const state = character(INITIAL_STATE, listSuccess(data));
    expect(state).toStrictEqual({
      page: 2,
      name: '',
      characters: data,
      loading: false,
      hasMore: false,
    });
  });

  it('List listFailure', () => {
    const state = character(INITIAL_STATE, listFailure());
    expect(state).toStrictEqual({
      page: 1,
      name: '',
      characters: [],
      loading: false,
      hasMore: false,
    });
  });

  it('Edited list', () => {
    const data = [];
    for (let i = 0; i < PAGE; i += 1) {
      data.push({ id: i + 1, name: 'character' });
    }
    const state = character(INITIAL_STATE, editList(data));
    expect(state).toStrictEqual({
      page: 1,
      name: '',
      characters: data,
      loading: false,
      hasMore: false,
    });
  });
});
