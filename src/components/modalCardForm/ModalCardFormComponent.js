import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import { editList } from '../../store/modules/character/actions';

import {
  setCharacter as storedCharacter,
  compareEdited,
  getImageUrl,
} from '../../services/character';

import COLORS from '../../constants/colors';

import InputComponent from '../input/InputComponent';
import ButtonComponent from '../button/ButtonComponent';
import { Container, Content, ContainerSerie } from './styles';

function ModalCardFormComponent({ character, close }) {
  const [name, setName] = useState('');
  const [image, setImage] = useState('');
  const [imageError, setImageError] = useState(false);
  const [description, setDescription] = useState('');
  const [serie, setSerie] = useState('');
  const [series, setSeries] = useState([]);

  const characters = useSelector((state) => state.character.characters);
  const dispatch = useDispatch();

  const handleRemove = (nameSerie) => {
    const newSeries = series.filter((value) => value.name !== nameSerie);
    setSeries(newSeries);
  };

  const handleAddSerie = () => {
    if (serie) setSeries([{ name: serie }, ...series]);
    setSerie('');
  };

  const handleSave = (event) => {
    event.preventDefault();
    if (imageError) {
      toast.error('Desculpe mas a url da imagem é inválida');
      return;
    }
    const newCharacter = {
      ...character,
      name,
      description,
      image,
      series: { ...character.series, items: [...series] },
    };
    storedCharacter(newCharacter);
    const newCharacters = compareEdited(characters);
    dispatch(editList(newCharacters));
    toast.success('Personagem editado com sucesso.');
    close();
  };

  const handleImage = (value) => {
    setImage(value);
  };
  const handleImageError = (value) => {
    setImageError(value);
  };
  const handleName = (value) => {
    setName(value);
  };
  const handleDescription = (value) => {
    setDescription(value);
  };
  const handleSerie = (value) => {
    setSerie(value);
  };

  useEffect(() => {
    if (character) {
      setName(character.name);
      setDescription(character.description);
      setSeries(character.series.items);
      setImage(getImageUrl(character));
    }
    window.scrollTo(0, 0);
  }, [character]);

  return (
    <Container>
      <Content>
        <h3>Editar {character.name}</h3>

        <form data-testid="form" onSubmit={handleSave}>
          <img
            src={image}
            onError={() => handleImageError(true)}
            onLoad={() => handleImageError(false)}
            alt=""
          />
          <section>
            <InputComponent
              name="image"
              placeholder="Cole o link da imagem"
              value={image}
              error={imageError}
              labelName="Imagem"
              help="Vá ao google imagens, copie o endereço da imagem e cole no campo"
              handleChange={handleImage}
            />
            <ButtonComponent
              color={COLORS.PRIMARY}
              remove
              action={() => handleImage('')}
            />
          </section>
          <InputComponent
            name="name"
            placeholder="Nome do personagem"
            value={name}
            error={name.length < 1}
            labelName="Nome"
            handleChange={handleName}
          />
          <InputComponent
            name="description"
            placeholder="Descrição do personagem"
            value={description}
            labelName="Descrição"
            handleChange={handleDescription}
            textArea
          />

          <section>
            <InputComponent
              name="serie"
              placeholder="Nome da Série"
              help="preenha o nome e clique no botão de mais"
              value={serie}
              labelName="Adicionar Série"
              handleChange={handleSerie}
            />
            <ButtonComponent add action={handleAddSerie} />
          </section>
          {series.map((value) => (
            <ContainerSerie key={value.name}>
              <p>{value.name}</p>
              <ButtonComponent
                color={COLORS.DANGER}
                remove
                small
                action={() => handleRemove(value.name)}
              />
            </ContainerSerie>
          ))}
          <footer>
            <ButtonComponent
              color={COLORS.GRAY}
              action={close}
              cancel
              text="Cancelar"
            />
            <ButtonComponent
              color={COLORS.SECONDARY_DARK}
              save
              text="Salvar"
              type="submit"
            />
          </footer>
        </form>
      </Content>
    </Container>
  );
}

ModalCardFormComponent.propTypes = {
  character: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ])
  ).isRequired,
  close: PropTypes.func.isRequired,
};

export default ModalCardFormComponent;
