import styled from 'styled-components';
import COLORS from '../../constants/colors';

export const Container = styled.div`
  width: 100%;
  padding: 0 10px;
`;
export const Contant = styled.div`
  width: 100%;
  background: ${COLORS.WHITE};
  box-shadow: 0 1px 4px 0 ${COLORS.OPACITY};
  border-radius: 3px;
  margin-top: 10px;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;

  > div {
    display: flex;
    justify-content: space-between;
  }

  @media (max-width: 600px) {
    flex-direction: column;

    > div {
      width: 100%;
      > button {
        flex: 1;
        margin-top: 10px;
      }
    }
  }
`;
