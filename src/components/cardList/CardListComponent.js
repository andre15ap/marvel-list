import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import InfiniteScroll from 'react-infinite-scroller';

import { listRequest } from '../../store/modules/character/actions';
import { getImageUrl } from '../../services/character';

import CardComponent from '../card/CardComponent';
import LoadingComponent from '../loading/LoadingComponent';
import { Container } from './styles';

function CardListComponent() {
  const characters = useSelector((state) => state.character.characters);
  const loading = useSelector((state) => state.character.loading);
  const page = useSelector((state) => state.character.page);
  const hasMore = useSelector((state) => state.character.hasMore);
  const name = useSelector((state) => state.character.name);
  const dispatch = useDispatch();

  const loadMore = () => {
    if (!loading) dispatch(listRequest(page, name));
  };

  const renderCharacters = () => {
    if (characters.length) {
      return (
        <>
          <InfiniteScroll
            initialLoad={false}
            pageStart={0}
            loadMore={loadMore}
            hasMore={hasMore}
          >
            {characters.map((value) => (
              <CardComponent
                key={value.id}
                id={value.id}
                name={value.name}
                imageUrl={getImageUrl(value)}
              />
            ))}
          </InfiniteScroll>
          {hasMore && <LoadingComponent />}
        </>
      );
    }

    return loading ? <LoadingComponent /> : <h3>Lista Vazia</h3>;
  };

  useEffect(() => {
    if (page === 1) dispatch(listRequest(page));
    // eslint-disable-next-line
  }, []);

  return <Container>{renderCharacters()}</Container>;
}

export default CardListComponent;
