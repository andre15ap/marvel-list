/* eslint-disable no-undef */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import ButtonComponent from '../../components/button/ButtonComponent';

describe('Button Component', () => {
  it('Should render text and call action', () => {
    const funcAction = jest.fn();

    const { getByText } = render(
      <ButtonComponent text="action" action={funcAction} />
    );

    const button = getByText('action');
    fireEvent.click(button);
    expect(funcAction).toHaveBeenCalledTimes(1);

    for (let i = 0; i < 3; i += 1) {
      fireEvent.click(button);
    }

    expect(funcAction).toHaveBeenCalledTimes(4);
  });
});
