/* eslint-disable no-undef */
import React from 'react';
import { render } from '@testing-library/react';

import CharacterDetailComponent from '../../components/characterDetail/CharacterDetailComponent';

describe('CharacterDetail Component', () => {
  it('Should render detail of character and action', () => {
    const fakeCharacter = {
      id: 1,
      name: 'Spider Man',
      description: 'Super Hero',
      thumbnail: { path: 'http:link.com', extension: 'png' },
      series: { items: [{ name: 'Amazing Spider man' }] },
    };
    const handleEdit = jest.fn();

    const { getByText, getAllByAltText } = render(
      <CharacterDetailComponent
        character={fakeCharacter}
        handleEdit={handleEdit}
      />
    );

    const name = getByText(fakeCharacter.name);
    expect(name).toBeTruthy();
    const image = getAllByAltText('character');
    expect(image).toBeTruthy();
    const description = getByText(fakeCharacter.description);
    expect(description).toBeTruthy();
    const serie = getByText(fakeCharacter.series.items[0].name);
    expect(serie).toBeTruthy();
  });
});
