import styled from 'styled-components';

import COLORS from '../../constants/colors';

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  > input {
    min-height: 44px;
    padding: 10px 15px;
    color: ${(props) => (props.error ? COLORS.BLACK : COLORS.PRIMARY)};
    border: solid 1px
      ${(props) => (props.error ? COLORS.DANGER : COLORS.PRIMARY)};
    border-radius: 4px;
    flex: 1;
    background: ${(props) => props.background};
  }

  > textarea {
    padding: 10px;
    color: ${COLORS.PRIMARY};
    border: solid 1px;
    border-radius: 4px;
    resize: none;
  }

  > label {
    margin-top: 10px;
    margin-bottom: 3px;
    font-size: 12px;
    font-weight: bold;
    align-self: flex-start;
    color: ${(props) => (props.error ? COLORS.DANGER : COLORS.PRIMARY)};
  }

  > span {
    font-size: 1.2rem;
    text-align: left;
  }
`;
