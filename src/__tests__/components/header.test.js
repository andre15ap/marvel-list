/* eslint-disable no-undef */
import React from 'react';
import { render } from '@testing-library/react';

import HeaderComponent from '../../components/header/HeaderComponent';

describe('Header Component', () => {
  it('Should render Header itens', () => {
    const { getByText, getByAltText } = render(
      <HeaderComponent title="Marvel List" />
    );

    const title = getByText('Marvel List');
    expect(title).toBeTruthy();
    const image = getByAltText('logo');
    expect(image).toBeTruthy();
  });
});
