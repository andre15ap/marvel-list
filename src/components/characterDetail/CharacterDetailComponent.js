import React from 'react';
import PropTypes from 'prop-types';

import { getImageUrl } from '../../services/character';

import ButtonComponent from '../button/ButtonComponent';
import { Container, ContainerColumn } from './styles';

function CharacterDetailComponent({ character, handleEdit }) {
  return (
    <Container>
      <ContainerColumn>
        <img src={getImageUrl(character)} alt="character" />
        <strong>{character.name}</strong>
        <span>{character.description}</span>
        <ButtonComponent edit action={handleEdit} text="Editar" />
      </ContainerColumn>
      <ContainerColumn>
        <h4>Séries</h4>
        {character.series.items.map((value) => (
          <p key={`${value.name}${Math.random()}`}>{value.name}</p>
        ))}
      </ContainerColumn>
    </Container>
  );
}

CharacterDetailComponent.propTypes = {
  handleEdit: PropTypes.func.isRequired,
  character: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ])
  ).isRequired,
};

export default CharacterDetailComponent;
