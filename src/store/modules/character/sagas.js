import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import api from '../../../services/api';
import { compareEdited } from '../../../services/character';
import apiKey from '../../../config/apiKey';
import getPage from '../../../config/getPage';
import { listFailure, listSuccess } from './actions';
import TYPES from './types';

export function* caractersRequest({ payload }) {
  try {
    const { page = 1, name = '' } = payload;
    // console.log(`page: ${page} - name: ${name}`);
    const params = name
      ? `/characters?nameStartsWith=${name}&${getPage(page)}&${apiKey}`
      : `/characters?${getPage(page)}&${apiKey}`;

    const response = yield call(api.get, params);

    const data = compareEdited(response.data.data.results);

    yield put(listSuccess(data));
  } catch (err) {
    if (err.response) {
      toast.error(err.response.data.error);
    } else {
      toast.error('Error, Verifique sua conexão');
    }
    yield put(listFailure());
  }
}

export default all([
  takeLatest(TYPES.CHARACTER_LIST_REQUEST, caractersRequest),
]);
