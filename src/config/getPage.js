import PAGE from '../constants/pagination';

function getPage(page) {
    const offset = (page - 1) * PAGE;
    const limit = PAGE;

    return `offset=${offset}&limit=${limit}`;
}

export default getPage;