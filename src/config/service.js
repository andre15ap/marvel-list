const URL =
  process.env.NODE_ENV === 'development'
    ? 'http://gateway.marvel.com/v1/public'
    : 'http://gateway.marvel.com/v1/public';

export default URL;