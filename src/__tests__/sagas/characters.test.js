/* eslint-disable no-undef */
import { runSaga } from 'redux-saga';
import MockAdapter from 'axios-mock-adapter';
import {
  listSuccess,
  listFailure,
} from '../../store/modules/character/actions';

import { caractersRequest } from '../../store/modules/character/sagas';
import api from '../../services/api';

import apiKey from '../../config/apiKey';
import getPage from '../../config/getPage';

const apiMock = new MockAdapter(api);

describe('Sagas character', () => {
  it('Should be able to fetch characters success', async () => {
    const dispatch = jest.fn();

    const response = {
      data: { results: [{ id: 1, name: 'character' }] },
    };

    apiMock.onGet(`/characters?${getPage(1)}&${apiKey}`).reply(200, response);

    await runSaga({ dispatch }, caractersRequest, {
      payload: { page: 1 },
    }).toPromise();

    expect(dispatch).toHaveBeenCalledWith(
      listSuccess([{ id: 1, name: 'character' }])
    );
  });

  it('Should be able to fetch characters failure', async () => {
    const dispatch = jest.fn();

    const response = {
      error: 'erro interno',
    };

    apiMock.onGet(`/characters?${getPage(1)}&${apiKey}`).reply(200, response);

    await runSaga({ dispatch }, caractersRequest, {
      payload: { page: 1 },
    }).toPromise();

    expect(dispatch).toHaveBeenCalledWith(listFailure());
  });
});
