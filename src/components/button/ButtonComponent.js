import React from 'react';
import PropTypes from 'prop-types';

import {
  FaTimes,
  FaRegSave,
  FaRegEdit,
  FaReply,
  FaSearchPlus,
  FaSearchMinus,
  FaRegPlusSquare,
} from 'react-icons/fa';
import COLORS from '../../constants/colors';

import { Button } from './styles';

function ButtonComponent({
  text,
  action,
  small,
  color,
  textColor,
  type,
  remove,
  save,
  edit,
  cancel,
  search,
  clean,
  add,
}) {
  return (
    <Button
      small={small}
      onClick={action}
      color={color}
      textColor={textColor}
      type={type}
    >
      {remove && <FaTimes color={COLORS.WHITE} size={small ? 15 : 18} />}
      {save && <FaRegSave color={COLORS.WHITE} size={small ? 15 : 18} />}
      {edit && <FaRegEdit color={COLORS.WHITE} size={small ? 15 : 18} />}
      {cancel && <FaReply color={COLORS.WHITE} size={small ? 15 : 18} />}
      {search && <FaSearchPlus color={COLORS.WHITE} size={small ? 15 : 18} />}
      {clean && <FaSearchMinus color={COLORS.WHITE} size={small ? 15 : 18} />}
      {add && <FaRegPlusSquare color={COLORS.WHITE} size={small ? 15 : 18} />}
      {text}
    </Button>
  );
}

ButtonComponent.propTypes = {
  text: PropTypes.string,
  action: PropTypes.func,
  small: PropTypes.bool,
  type: PropTypes.string,
  color: PropTypes.string,
  textColor: PropTypes.string,
  remove: PropTypes.bool,
  save: PropTypes.bool,
  edit: PropTypes.bool,
  cancel: PropTypes.bool,
  search: PropTypes.bool,
  clean: PropTypes.bool,
  add: PropTypes.bool,
};

ButtonComponent.defaultProps = {
  text: '',
  type: 'button',
  action: null,
  small: false,
  color: '',
  textColor: '',
  remove: false,
  save: false,
  edit: false,
  cancel: false,
  search: false,
  clean: false,
  add: false,
};

export default ButtonComponent;
