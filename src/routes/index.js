import React from 'react';
import { Route, Switch } from 'react-router-dom';

import HomePage from '../pages/home/HomePage';
import DetailPage from '../pages/detail/DetailPage';

function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={HomePage} />
      <Route path="/:id" component={DetailPage} />
    </Switch>
  );
}

export default Routes;
