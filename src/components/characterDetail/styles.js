import styled from 'styled-components';

import COLORS from '../../constants/colors';

export const Container = styled.div`
  width: 100%;
  max-width: 900px;
  display: flex;
  padding: 10px;
  min-height: calc(100% - 64px);

  @media (max-width: 600px) {
    flex-direction: column;

    > div {
      max-width: 100%;
    }
  }
`;

export const ContainerColumn = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
  width: 100%;
  max-width: 50%;
  background: ${COLORS.WHITE};
  border-radius: 3px;

  > img {
    max-width: 100%;
    max-height: 550px;
    width: auto;
    height: auto;
    border-radius: 5px;
  }

  > strong {
    color: ${COLORS.PRIMARY_DARK};
    margin: 5px 0;
  }

  > h4 {
    margin-bottom: 5px;
  }

  > p {
    border-radius: 3px;
    margin-bottom: 5px;
    border: 1px solid ${COLORS.PRIMARY_TRANSPARENT};
    width: 100%;
    padding: 8px 5px;
    font-size: 1.5rem;
  }

  > span {
    text-align: justify;
    font-size: 1.4rem;
    margin-bottom: 10px;
  }
`;
