import styled from 'styled-components';

import COLORS from '../../constants/colors';

export const Container = styled.div`
  background: ${COLORS.PRIMARY};
  padding: 0 30px;
  position: absolute;
  width: 100%;
`;

export const Content = styled.div`
  height: 64px;
  max-width: 900px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;

  > button {
    color: ${COLORS.WHITE};
    font-weight: bold;
    display: flex;
    align-items: center;
    background: none;
    border: none;

    > img {
      max-height: 40px;
      margin-right: 15px;
      padding-right: 15px;
      color: ${COLORS.WHITE};
      border-right: 1px solid ${COLORS.WHITE};
    }
  }

  > aside {
    display: flex;
    align-items: center;

    > button {
      margin: 0 5px;
      font-size: 1.6rem;
      color: ${COLORS.WHITE};
      font-weight: bold;
      background: none;
      border: none;
    }
  }
`;
