import styled from 'styled-components';

import COLORS from '../../constants/colors';

export const Container = styled.button`
  box-shadow: 0 4px 8px 0 ${COLORS.OPACITY};
  border-radius: 5px;
  transition: 0.4s;
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
  border: 0;
  background: ${COLORS.GRAY};
  padding: 10px;

  :hover {
    box-shadow: 0 8px 16px 0 ${COLORS.OPACITY};

    > div {
      border-radius: 5px;
      > img {
        width: 100%;
        height: 100%;
        border-radius: 5px;
      }
    }
    > section {
      > p {
        font-size: 1.45rem;
      }
    }
  }

  > div {
    position: relative;
    max-width: 180px;
    height: 200px;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    > img {
      transition: 0.3s ease-in-out;
      height: 95%;
      width: 95%;
      border-radius: 3px;
    }

    @media (max-width: 600px) {
      max-width: 250px;
      height: 300px;
    }
  }

  > section {
    position: relative;
    height: 4rem;
    > p {
      margin: 10px 0;
      font-size: 1.4rem;
      font-weight: bold;
      transition: 0.3s ease-in-out;
      line-height: 1;
      color: ${COLORS.WHITE};
    }
  }
`;
