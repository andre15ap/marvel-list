import styled from 'styled-components';

import COLORS from '../../constants/colors';

export const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  padding-top: 64px;
  justify-content: center;
  background: ${(props) =>
    props.background ? COLORS.BACKGROUND : COLORS.WHITE};
`;
