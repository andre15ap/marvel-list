import styled from 'styled-components';

import COLORS from '../../constants/colors';

export const Button = styled.button`
  height: ${(props) => (props.small ? 30 : 44)}px;
  padding: 0 10px;
  margin: 0 5px;
  box-shadow: 0 4px 8px 0 ${COLORS.OPACITY};
  background: ${(props) => props.color || COLORS.PRIMARY};
  color: ${(props) => props.textColor || COLORS.WHITE};
  font-weight: bold;
  border: 0;
  border-radius: 4px;
  transition: background 0.2s;
  display: flex;
  justify-content: center;

  > svg {
    margin: 0 5px;
  }

  &:hover {
    opacity: 0.8;
  }
`;
