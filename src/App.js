import React from 'react';

import { Router } from 'react-router-dom';

import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import history from './services/history';
import Routes from './routes';
import GlobalStyle from './styles/global';
import store from './store';

import HeaderComponent from './components/header/HeaderComponent';

function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <HeaderComponent title="Personagens Marvel" />
        <Routes />
        <GlobalStyle />
        <ToastContainer autoClose={3000} />
      </Router>
    </Provider>
  );
}

export default App;
