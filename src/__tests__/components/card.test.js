/* eslint-disable no-undef */
import React from 'react';
import { render } from '@testing-library/react';

import CardComponent from '../../components/card/CardComponent';

describe('Card Component', () => {
  it('Should render card character', () => {
    const { getByText, getByAltText } = render(
      <CardComponent
        id={1}
        name="Spider Man"
        imageUrl="http://ulrfake.com/image.jpg"
      />
    );

    const name = getByText('Spider Man');
    expect(name).toBeTruthy();
    const image = getByAltText('character');
    expect(image).toBeTruthy();
  });

  it('Should render card character name long', () => {
    const { getByText, getByAltText } = render(
      <CardComponent
        id={1}
        name="Spider Man ultimate spider black edition"
        imageUrl="http://ulrfake.com/image.jpg"
      />
    );

    const text = getByText(`Spider Man ultimate spider ...`);
    expect(text).toBeTruthy();
    const image = getByAltText('character');
    expect(image).toBeTruthy();
  });
});
