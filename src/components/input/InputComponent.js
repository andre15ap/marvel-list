import React from 'react';
import PropTypes from 'prop-types';

import COLORS from '../../constants/colors';

import { Container } from './styles';

function InputComponent({
  name,
  value,
  placeholder,
  labelName,
  help,
  handleChange,
  error,
  textArea,
  background,
}) {
  return (
    <Container error={error} background={background}>
      <label htmlFor={name}>{labelName}</label>
      <span>{help}</span>
      {textArea ? (
        <textarea
          name={name}
          id={name}
          onChange={(val) => handleChange(val.target.value)}
          value={value}
          rows="4"
          placeholder={placeholder}
        />
      ) : (
        <input
          name={name}
          id={name}
          value={value}
          placeholder={placeholder}
          onChange={(val) => handleChange(val.target.value)}
        />
      )}
    </Container>
  );
}

InputComponent.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  labelName: PropTypes.string,
  help: PropTypes.string,
  error: PropTypes.bool,
  textArea: PropTypes.bool,
  background: PropTypes.string,
};

InputComponent.defaultProps = {
  placeholder: '',
  labelName: '',
  help: '',
  background: COLORS.WHITE,
  error: false,
  textArea: false,
};

export default InputComponent;
