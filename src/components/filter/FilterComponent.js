import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { listRequest } from '../../store/modules/character/actions';

import InputComponent from '../input/InputComponent';
import ButtonComponent from '../button/ButtonComponent';
import { Container, Contant } from './styles';

function FilterComponent() {
  const [search, setSearch] = useState('');
  const name = useSelector((state) => state.character.name);

  const dispatch = useDispatch();

  const handleSeach = () => {
    dispatch(listRequest(1, search));
  };

  const handleClean = () => {
    setSearch('');
    dispatch(listRequest(1, ''));
  };

  useEffect(() => {
    setSearch(name);
  }, [name]);

  return (
    <Container>
      <Contant>
        <InputComponent
          name="search"
          labelName="Filtrar por Nome"
          value={search}
          placeholder="Buscar por inicio de nome"
          handleChange={(value) => setSearch(value)}
        />
        <div>
          <ButtonComponent text="Buscar" search action={handleSeach} />
          <ButtonComponent text="Limpar" clean action={handleClean} />
        </div>
      </Contant>
    </Container>
  );
}

export default FilterComponent;
