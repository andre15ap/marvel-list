import styled from 'styled-components';
import COLORS from '../../constants/colors';

export const Container = styled.div`
  display: flex;
  position: absolute;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  min-height: 100vh;
  width: 100%;
  background: ${COLORS.PRIMARY_TRANSPARENT};
  text-align: center;
  z-index: 3;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 700px;
  background: ${COLORS.WHITE};
  margin: 20px 0 20px 0;
  padding: 20px;
  border-radius: 5px;

  > h2 {
    font-size: 22px;
    margin: 10px 0;
    font-weight: bold;
    color: ${COLORS.PRIMARY};
  }

  form {
    display: flex;
    flex-direction: column;

    > img {
      width: 300px;
      height: 350px;
      align-self: center;
      margin-top: 10px;
      border-radius: 5px;
      border: solid 1px ${COLORS.PRIMARY_TRANSPARENT};
    }

    > section {
      display: flex;
      width: 100%;
      justify-content: space-between;
      align-items: flex-end;
    }

    > footer {
      display: flex;
      justify-content: flex-end;
      margin-top: 10px;
    }
  }
`;

export const ContainerSerie = styled.li`
  border-radius: 3px;
  margin-top: 10px;
  border: 1px solid ${COLORS.PRIMARY_TRANSPARENT};
  width: 100%;
  padding: 8px 5px;
  font-size: 1.5rem;
  display: flex;
  justify-content: space-between;
`;
